﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(NPCMove))]
public class NPCWaypointEditor : Editor {

    private const float handleSize = 0.04f;
    private const float pickSize = 0.06f;

    private int selectedIndex = -1;
    private Transform handleTransform;
    private Quaternion handleRotation;

    private NPCMove currentObject;

    private void OnEnable() {
        this.currentObject = this.target as NPCMove;
        this.handleTransform = this.currentObject.transform;
        this.handleRotation = this.currentObject.transform.localRotation;
    }

    private void OnDisable() {
        this.currentObject = null;
    }

    void OnSceneGUI() {
        List<Waypoint> waypoints = this.currentObject.waypoints;
        for(int i = 0; i < waypoints.Count; i++) {
            this.ShowPoint(i);
        }
        this.DrawDirection();
    }

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        EditorGUILayout.BeginVertical();

        //this.SpeedForm();
        //this.FirstWaitForm();
        this.WaypointSizeForm();
        this.WaypointList();

        EditorGUILayout.EndVertical();
    }

    private void SpeedForm() {
        EditorGUILayout.BeginVertical();

        EditorGUI.BeginChangeCheck();
        float speed = EditorGUILayout.FloatField("Speed", this.currentObject.speed);
        if(EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(this.target, "Speed changed");
            EditorUtility.SetDirty(this.target);
            this.currentObject.speed = speed;
        }
        EditorGUILayout.EndVertical();
    }

    private void FirstWaitForm() {
        EditorGUILayout.BeginVertical();

        EditorGUI.BeginChangeCheck();
        float waitTime = EditorGUILayout.FloatField("Wait time", this.currentObject.firstWait);
        if(EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(this.target, "wait time changed");
            EditorUtility.SetDirty(this.target);
            this.currentObject.firstWait = waitTime;
        }
        EditorGUILayout.EndVertical();
    }

    private void WaypointSizeForm() {
        EditorGUILayout.BeginVertical();
        EditorGUI.BeginChangeCheck();
        int count = EditorGUILayout.IntField("Waypoint Count", this.currentObject.waypoints.Count);
        if(EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(this.target, "waypoint size changed");
            EditorUtility.SetDirty(this.target);
            if(count > this.currentObject.waypoints.Count) {
                for(int i = this.currentObject.waypoints.Count; i < count; i++) {
                    this.currentObject.waypoints.Add(new Waypoint(Vector2.zero, 0.0f));
                }
            } else {
                this.currentObject.waypoints.RemoveRange(count, this.currentObject.waypoints.Count - count);
            }
        }
        EditorGUILayout.EndVertical();
    }

    private void WaypointList() {
        EditorGUILayout.BeginVertical();
        for(int i = 0; i < this.currentObject.waypoints.Count; i++) {
            EditorGUILayout.Separator();
            EditorGUI.BeginChangeCheck();
            Vector2 vector = EditorGUILayout.Vector2Field("Waypoint Position " + i, this.currentObject.waypoints[i].position);
            if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(this.target, "waypoint position changed");
                EditorUtility.SetDirty(this.target);
                this.currentObject.waypoints[i].position = vector;
            }

            EditorGUI.BeginChangeCheck();
            float delay = EditorGUILayout.FloatField("delay for point " + i, this.currentObject.waypoints[i].waitTime);
            if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(this.target, "waypoint position changed");
                EditorUtility.SetDirty(this.target);
                this.currentObject.waypoints[i].waitTime = delay;
            }
        }
        EditorGUILayout.EndVertical();
    }

    private void ShowPoint(int index) {
        Vector2 point = this.currentObject.waypoints[index].position;
        float size = HandleUtility.GetHandleSize(point);
        Handles.color = Color.magenta;

        Handles.Label(point - new Vector2(0.05f, 0.05f), index.ToString());

        if(Handles.Button(point, this.handleRotation, 0.3f * handleSize, 0.3f * pickSize, Handles.DotHandleCap)) {
            this.selectedIndex = index;
            this.Repaint();
        }

        if(this.selectedIndex == index) {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, this.handleRotation);

            if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(this.currentObject, "Move Point");
                EditorUtility.SetDirty(this.currentObject);
                this.currentObject.waypoints[index].position = point;
            }
        }
    }

    private void DrawDirection() {
        Handles.color = Color.green;

        for(int i = 0; i < this.currentObject.waypoints.Count - 1; i++) {
            if(i == 0) {
                Vector3 pgo = this.currentObject.gameObject.transform.position;
                Handles.DrawLine(pgo, this.currentObject.waypoints[i].position);
            }
            Vector3 p1 = this.currentObject.waypoints[i].position;
            Vector3 p2 = this.currentObject.waypoints[i + 1].position;
            Handles.DrawLine(p1, p2);
        }
    }
}

