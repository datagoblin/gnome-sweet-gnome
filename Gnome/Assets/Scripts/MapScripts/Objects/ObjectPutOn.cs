﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPutOn : ObjectInteractable{

    public ObjectPickUp objectToReceive;
    public Vector2 placedObjectPosition = new Vector2(0,0.1f);
    public bool receiving = true;
    public GameObject playerCharacter;


    private void Update() {
        if (playerCharacter.GetComponent<PlayerInteract>().carriedObject == objectToReceive.gameObject && !receiving) {
            receiving = true;
            currentlyInteractible = true;
        }
        if (playerCharacter.GetComponent<PlayerInteract>().carriedObject != objectToReceive.gameObject && receiving) {
            receiving = false;
            currentlyInteractible = false;
        }
    }

    public override void Interact(GameObject player) {
        if(receiving && player.GetComponent<PlayerInteract>().carriedObject == objectToReceive.gameObject) {
            objectToReceive.gameObject.transform.SetParent(transform);
            objectToReceive.gameObject.transform.localPosition = placedObjectPosition;
            objectToReceive.gameObject.transform.rotation = Quaternion.identity;
            player.GetComponent<PlayerInteract>().carriedObject = null;
            receiving = false;
            currentlyInteractible = false;
            CentralTriggers.SharedInstance.Trigger(TriggerEnumeration.PlayerDroppedItem);
            Complete();
        }
    }

}
