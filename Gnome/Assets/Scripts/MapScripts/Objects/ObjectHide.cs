﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHide : ObjectInteractable{
    public bool playerInside = false;

    Sprite baseSprite;
    public Sprite hiddenSprite;
    public Sprite hatSprite;

    private void Start() {
        baseSprite = GetComponent<SpriteRenderer>().sprite;
    }

    public override void Interact(GameObject player) {
        if(!playerInside && !player.GetComponent<PlayerFreeMovement>().isHidden) {
            playerInside = true;
            player.GetComponent<PlayerFreeMovement>().isHidden = true;
            player.GetComponent<PlayerFreeMovement>().Stop();
            player.GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<SpriteRenderer>().sprite = hiddenSprite;
            iconPrefab.GetComponent<SpriteRenderer>().sprite = hatSprite;
        } else {
            playerInside = false;
            player.GetComponent<PlayerFreeMovement>().isHidden = false;
            player.GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<SpriteRenderer>().sprite = baseSprite;
            iconPrefab.GetComponent<SpriteRenderer>().sprite = interactionIcon;
        }

    }

}
