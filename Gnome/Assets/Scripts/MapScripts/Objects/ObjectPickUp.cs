﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPickUp : ObjectInteractable{

    public bool isPicked = false;
    Transform origParent;

    private void Start() {
        origParent = gameObject.transform.parent;

    }

    override public void Interact(GameObject player) {
        if (!isPicked) {
            this.Pick(player);
        } else {
            this.Drop(player);
        }
    }

    private void Pick(GameObject player) {
        player.GetComponent<PlayerInteract>().carriedObject = gameObject;
        gameObject.transform.SetParent(player.transform);
        gameObject.transform.localPosition = new Vector3(0, player.GetComponent<Collider2D>().bounds.extents.y * 1.5f, 0);
        isPicked = true;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().sortingOrder += 1;
        iconPrefab.GetComponent<SpriteRenderer>().enabled = false;
        CentralTriggers.SharedInstance.Trigger(TriggerEnumeration.PlayerPickedItem);
    }

    private void Drop(GameObject player) {
        if (player.GetComponent<PlayerInteract>().carriedObject != null) {
            player.GetComponent<PlayerInteract>().carriedObject = null;
            gameObject.transform.localPosition = new Vector3(player.GetComponent<Collider2D>().bounds.extents.x, 0, 0);
            gameObject.transform.rotation = Quaternion.identity;
            gameObject.transform.SetParent(origParent);
            isPicked = false;
            GetComponent<BoxCollider2D>().enabled = true;
            GetComponent<SpriteRenderer>().sortingOrder -= 1;
            iconPrefab.GetComponent<SpriteRenderer>().enabled = true;
            CentralTriggers.SharedInstance.Trigger(TriggerEnumeration.PlayerDroppedItem);
        }
    }
}
