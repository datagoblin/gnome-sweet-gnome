﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInteractable : MonoBehaviour{
    public GameObject iconPrefab;
    public Sprite interactionIcon;
    public bool currentlyInteractible = true;

    public bool mainQuest = false;
    public bool completed = false;

    public void ShowIcon() {
        iconPrefab.GetComponent<SpriteRenderer>().sprite = interactionIcon;
    }

    public void HideIcon() {
        iconPrefab.GetComponent<SpriteRenderer>().sprite = null;
    }

    virtual public void Interact(GameObject player) {

    }

    public void Complete() {
        if(completed == false) {
            completed = true;

            if(mainQuest == true) {
                Debug.Log("Main objective completed");
            } else {
                Debug.Log("Optional objective completed");
            }

            GameObject.FindGameObjectWithTag("GameController").GetComponent<GamePersonWhoDoesManagement>().CheckMainObjectives();
        }
    }
}
