﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFix : ObjectInteractable{
    Sprite originalSprite;
    public Sprite fixedSprite;
    bool isFixed = false;

    private void Start() {
        originalSprite = GetComponent<SpriteRenderer>().sprite;
    }

    public override void Interact(GameObject player) {
        if (!isFixed) {
            Debug.Log("FIXED");
            CentralTriggers.SharedInstance.Trigger(TriggerEnumeration.PlayerFixedItem);
            GetComponent<SpriteRenderer>().sprite = fixedSprite;
            isFixed = true;
            currentlyInteractible = false;
            Complete();
        }
    }

}
