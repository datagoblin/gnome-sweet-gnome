﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Text.RegularExpressions;

//GameManager is cursed name
public class GamePersonWhoDoesManagement : MonoBehaviour, ITriggerObserver{
    public float loseAnimationTime = 1.0f;
    public SceneEnum loseScene = SceneEnum.None;
    public SceneEnum winScene = SceneEnum.None;

    private Animator npcAnm;
    private Animator pcAnm;

    private void Start() {
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerFled);

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject npc = GameObject.FindGameObjectWithTag("NPC");

        SceneCoordinator.lastPlayedStage = SceneCoordinator.CurrentStage;

        pcAnm = player.GetComponent<Animator>();
        if (npc != null) {
            npcAnm = npc.GetComponent<Animator>();
        }
    }

    private void OnDestroy() {
        CentralTriggers.SharedInstance.RemoveAllObservers();
    }

    public bool CheckMainObjectives() {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Interact");

        foreach(GameObject obj in objs) {
            if (obj.GetComponent<ObjectInteractable>().mainQuest) {
                if (!obj.GetComponent<ObjectInteractable>().completed) {
                    Debug.Log("Main quest NOT completed.");
                    return false;
                }
            }
        }

        Debug.Log("Main quest COMPLETED -complete main quest event here-");
        CentralTriggers.SharedInstance.Trigger(TriggerEnumeration.ObjectivesComplete);
        return true;
    }

    public void Lose() {
        Debug.Log("POOF YERSELF -lose event here-");
        CentralTriggers.SharedInstance.PARATUDO();
        StartCoroutine("CallLoseScene");

        npcAnm.Play("sally_spook");
        pcAnm.Play("poof");

    }

    public void Win() {
        Debug.Log("A WINNER IS YOU - win event here-");
        CentralTriggers.SharedInstance.PARATUDO();

        if (SceneCoordinator.CurrentStage == 4) {
            SceneManager.LoadScene(SceneCoordinator.SceneName(SceneEnum.CreditsScene));
        } else {
            SceneManager.LoadScene(SceneCoordinator.SceneName(this.winScene));
        }
     }

    public void OnTrigger(TriggerEnumeration trigger) {
        if(trigger == TriggerEnumeration.PlayerFled) {
            Win();
        }
    }

    IEnumerator CallLoseScene() {
        yield return new WaitForSeconds(this.loseAnimationTime);
        SceneManager.LoadScene(SceneCoordinator.SceneName(SceneEnum.LoseScene));
    }
}
