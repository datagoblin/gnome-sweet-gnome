﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralTriggers {
    private static CentralTriggers _instance;
    public static CentralTriggers SharedInstance {
        get {
            if(_instance == null) {
                _instance = new CentralTriggers();
            }
            return _instance;
        }
    }

    private readonly Dictionary<TriggerEnumeration, HashSet<ITriggerObserver>> observers = new Dictionary<TriggerEnumeration, HashSet<ITriggerObserver>>();
    private bool shouldClearObservers = false;

    public void PARATUDO() {
        this.shouldClearObservers = true;
    }

    public void RemoveAllObservers() {
        this.observers.Clear();
    }

    public void AddObserver(ITriggerObserver observer, TriggerEnumeration trigger) {
        if (trigger == TriggerEnumeration.None) { return; }
        if (!this.observers.ContainsKey(trigger)) {
            this.observers.Add(trigger, new HashSet<ITriggerObserver>());
        }
        this.observers[trigger].Add(observer);
    }

    public void RemoveObserver(ITriggerObserver observer, TriggerEnumeration trigger) {
        if (trigger == TriggerEnumeration.None) { return; }
        if (this.observers.ContainsKey(trigger)) {
            this.observers[trigger].Remove(observer);
        }
    }

    public void RemoveObserver(ITriggerObserver observer) {
        foreach(TriggerEnumeration entry in this.observers.Keys) {
            this.observers[entry].Remove(observer);
        }
    }

    public void Trigger(TriggerEnumeration trigger) {
        Debug.Log("Trigger: " + trigger.ToString());
        if (trigger == TriggerEnumeration.None) { return; }
        if(!this.observers.ContainsKey(trigger)) { return; }
        HashSet<ITriggerObserver> currentObservers = this.observers[trigger];
        foreach(ITriggerObserver obs in currentObservers) {
            obs.OnTrigger(trigger);
        }
        if(this.shouldClearObservers) {
            this.RemoveAllObservers();
        }
    }
}
