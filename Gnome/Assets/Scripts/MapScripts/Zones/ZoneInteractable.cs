﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneInteractable : ObjectInteractable, ITriggerObserver {

    public TriggerEnumeration triggerToEanble;
    public TriggerEnumeration triggerToPost;

    public void OnTrigger(TriggerEnumeration trigger) {
        if(trigger == triggerToEanble) {
            this.currentlyInteractible = true;
        }
    }

    void Start() {
        CentralTriggers.SharedInstance.AddObserver(this, this.triggerToEanble);
    }

    public override void Interact(GameObject player) {
        CentralTriggers.SharedInstance.Trigger(this.triggerToPost);
    }
}
