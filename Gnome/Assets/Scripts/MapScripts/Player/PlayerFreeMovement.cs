﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerFreeMovement : MonoBehaviour, ITriggerObserver {
    public bool isHidden = false;
    public float movementSpeed;
    public float mod = 0.2f;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D boxCollider;
    private readonly Dictionary<KeyCode, Vector3> keyMap = new Dictionary<KeyCode, Vector3>(){
        {KeyCode.UpArrow, Vector3.up},
        {KeyCode.DownArrow, Vector3.down},
        {KeyCode.LeftArrow, Vector3.left},
        {KeyCode.RightArrow, Vector3.right}
    };

    private void Start() {
        this.spriteRenderer = this.GetComponentInChildren<SpriteRenderer>();
        this.boxCollider = this.GetComponent<BoxCollider2D>();
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerDied);
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerFled);
    }

    void Update() {
        if (!isHidden) {
            this.PlayerInput();
        }
    }

    public void Stop() {
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
    }

    private void PlayerInput() {
        Vector3 direction = Vector3.zero;
        foreach (KeyValuePair<KeyCode, Vector3> entry in this.keyMap) {
            if(Input.GetKeyDown(entry.Key) || Input.GetKey(entry.Key)) {
                direction += entry.Value;
            }
        }
        Vector3 movementVector = (movementSpeed * Time.fixedDeltaTime) * direction;
        GetComponent<Rigidbody2D>().velocity = movementVector;
    }

    public void OnTrigger(TriggerEnumeration trigger) {
        this.Stop();
        this.enabled = false;
    }
}
