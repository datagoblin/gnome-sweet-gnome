﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpriteFlipper : MonoBehaviour {
    // Start is called before the first frame update
    private float characterXScale;
    void Start() {
        this.characterXScale = this.transform.localScale.x;
    }

    // Update is called once per frame
    void Update() {
        if (this.GetComponent<Rigidbody2D>().velocity.x > 0) {
            this.transform.localScale = new Vector2(-characterXScale, this.transform.localScale.y);
        } else if (this.GetComponent<Rigidbody2D>().velocity.x < 0) {
            this.transform.localScale = new Vector2(characterXScale, this.transform.localScale.y);
        }
    }
}
