﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour, ITriggerObserver {

    public GameObject closestObject;
    public GameObject carriedObject;
    public float interacitonRange = 1.0f;
    private GameObject prevObject;

    private void Start() {
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerDied);
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerFled);
    }

    private void Update() {
        UpdateClosestObject();
        if (Input.GetKeyDown(KeyCode.E)) {
            if (closestObject != null) {
                closestObject.GetComponent<ObjectInteractable>().Interact(gameObject);
            }
        }
    }

    private void UpdateClosestObject() {
        if(carriedObject != null) {
            GameObject putOn;
            if (HasPutOnInRange(out putOn)) {
                closestObject = putOn;
            } else {
                closestObject = carriedObject;
            }
        } else {
            GameObject interact;
            if (HasInteractableInRange(out interact)) {
                closestObject = interact;
            } else {
                closestObject = null;
            }
        }

        if(closestObject != prevObject) {
            if (prevObject != null) {
                prevObject.GetComponent<ObjectInteractable>().HideIcon();
            }
            if (closestObject != null) {
                closestObject.GetComponent<ObjectInteractable>().ShowIcon();
            }
        }
        prevObject = closestObject;
    }

    private bool HasPutOnInRange(out GameObject putOn) {
        GameObject[] interactables = GameObject.FindGameObjectsWithTag("Interact");
        foreach (GameObject obj in interactables) {
            float dist = Physics2D.Distance(gameObject.GetComponent<BoxCollider2D>(), obj.GetComponent<Collider2D>()).distance;
            if (dist <= interacitonRange && obj.GetComponent<ObjectInteractable>().currentlyInteractible == true && obj.GetComponent<ObjectPutOn>() != null && obj.GetComponent<ObjectPutOn>().objectToReceive.gameObject == carriedObject) {

                putOn = obj;
                return true;
            }
        }
        putOn = null;
        return false;
    }

    private bool HasInteractableInRange(out GameObject interactable) {
        GameObject[] interactables = GameObject.FindGameObjectsWithTag("Interact");
        GameObject closest = null;
        float closestDist = Mathf.Infinity;
        foreach(GameObject obj in interactables) {
            float dist = Physics2D.Distance(gameObject.GetComponent<BoxCollider2D>(), obj.GetComponent<Collider2D>()).distance;
            if (dist <= interacitonRange && obj.GetComponent<ObjectInteractable>().currentlyInteractible == true) {
                if (dist < closestDist) {
                    closest = obj;
                    closestDist = dist;
                }
            }
        }
        interactable = closest;
        return closest != null;
    }

    public void OnTrigger(TriggerEnumeration trigger) {
        this.enabled = false;
    }
}
