﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum AnimationStates {
    idle, moving, picking, fixing, movingCourier, idleCourier, diying
}

[RequireComponent(typeof(Animator))]
public class PlayerAnimationControl : MonoBehaviour, ITriggerObserver {
    private float characterXScale;
    private Animator animator;
    private Rigidbody2D ridigibody;
    private PlayerInteract interaction;

    void Start() {
        this.animator = this.GetComponent<Animator>();
        this.ridigibody = this.GetComponent<Rigidbody2D>();
        this.interaction = this.GetComponent<PlayerInteract>();
        this.SetupObservers();
        this.characterXScale = this.transform.localScale.x;
    }

    private void OnDestroy() {
        this.RemoveObservers();
    }

    void Update() {
        this.MoveAnimation();
    }

    void SetupObservers() {
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerPickedItem);
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerFixedItem);
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerDroppedItem);
        CentralTriggers.SharedInstance.AddObserver(this, TriggerEnumeration.PlayerDied);
    }

    void RemoveObservers() {
        CentralTriggers.SharedInstance.RemoveObserver(this);
    }

    void MoveAnimation() {
        bool isMoving = this.ridigibody.velocity != Vector2.zero;
        this.UpdateCharacterFacing();
        this.animator.SetBool("isMoving", isMoving);
    }
    
    void InteractionAnimation() {
        this.animator.SetTrigger("Interacting");
    }

    void PickingAnimation() {
        this.animator.SetTrigger("Pick");
        this.animator.SetBool("isCarrying", true);
    }

    void DroppingAnimation() {
        this.animator.SetBool("isCarrying", false);
    }

    public void OnTrigger(TriggerEnumeration trigger) {
        switch(trigger) {
            case TriggerEnumeration.PlayerFixedItem:
                this.InteractionAnimation();
                break;
            case TriggerEnumeration.PlayerPickedItem:
                this.PickingAnimation();
                break;
            case TriggerEnumeration.PlayerDroppedItem:
                this.DroppingAnimation();
                break;
        }
    }

    void UpdateCharacterFacing() {
        if(this.GetComponent<Rigidbody2D>().velocity.x > 0) {
            this.transform.localScale = new Vector2(-this.characterXScale, this.transform.localScale.y);
        } else if(this.GetComponent<Rigidbody2D>().velocity.x < 0) {
            this.transform.localScale = new Vector2(this.characterXScale, this.transform.localScale.y);
        }
    }
}
