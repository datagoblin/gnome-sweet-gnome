﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMove : MonoBehaviour {

    public float speed = 1.0f;
    public float firstWait = 0.0f;
    public bool loop = false;
    public List<Waypoint> waypoints = new List<Waypoint>();
    bool forward = true;
    int lastWaypointIndex = -1;
    public bool isMoving = false;
    private bool shouldWalkToNextWaypoint = true;
    private Animator anm;

    [HideInInspector]
    public Vector2 LatestDirection { get; private set; }

    void Start() {
        this.waypoints.Insert(0, new Waypoint(this.transform.position, this.firstWait));
        this.anm = this.GetComponent<Animator>();
    }

    void Update() {
        this.UpdateFacing();

        if(this.shouldWalkToNextWaypoint && this.waypoints.Count > 1) {
            if(this.lastWaypointIndex < 1) {
                this.forward = true;
            }
            if(this.lastWaypointIndex >= this.waypoints.Count - 1) {
                if(this.loop) {
                    this.lastWaypointIndex = -1;
                } else {
                    this.forward = false;
                }
            }
            this.StartCoroutine("MoveToNextWaypoint");
        }
    }

    IEnumerator MoveToNextWaypoint() {
        this.shouldWalkToNextWaypoint = false;
        this.isMoving = true;
        int nextIndex = this.forward ? this.lastWaypointIndex + 1 : this.lastWaypointIndex - 1;
        Vector2 p = this.waypoints[nextIndex].position;
        Vector2 direction = new Vector3(p.x, p.y, this.transform.position.z) - this.transform.position;
        this.LatestDirection = direction;
        while(direction.magnitude > 0.01f) {
            direction = new Vector3(p.x, p.y, this.transform.position.z) - this.transform.position;
            this.transform.Translate(direction.normalized * Time.fixedDeltaTime * this.speed);
            yield return null;
        }
        this.lastWaypointIndex = nextIndex;
        this.isMoving = false;
        yield return new WaitForSeconds(this.waypoints[nextIndex].waitTime);
        this.shouldWalkToNextWaypoint = true;
    }

    void UpdateFacing() {
        float angle = Mathf.Atan2(this.LatestDirection.x, this.LatestDirection.y) * Mathf.Rad2Deg;
        this.GetComponent<FieldOfView>().facingAngleOffset = angle;
    }
}

