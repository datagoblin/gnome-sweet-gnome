﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(NPCMove))]
public class NPCAnimationControl : MonoBehaviour {
    enum Direction {
        idle, up, down, left, right
    }

    private Animator animator;
    private NPCMove moveComponent;
    private Direction currentDirection = Direction.idle;

    void Start() {
        this.animator = this.GetComponent<Animator>();
        this.moveComponent = this.GetComponent<NPCMove>();
    }

    void FixedUpdate() {
        if(this.moveComponent.isMoving) {
            this.ProcessCharacterOrientation();
        } else if(this.currentDirection != Direction.idle) {
            this.currentDirection = Direction.idle;
            this.IdleAnimation();
        }
        this.UpdateIdleXScale();
    }

    void UpdateIdleXScale() {
        float xScale = (this.currentDirection == Direction.idle && this.moveComponent.LatestDirection.x > 0 ? -1 : 1);
        this.transform.localScale = new Vector3(xScale, 1, 1);
        this.GetComponentInChildren<MeshRenderer>().transform.localScale = new Vector3(1, 1, xScale);
    }

    void IdleAnimation() {
        this.animator.SetTrigger("sallyIdle");
    }

    void ProcessCharacterOrientation() {
        float angle = Mathf.Atan2(this.moveComponent.LatestDirection.x, this.moveComponent.LatestDirection.y) * Mathf.Rad2Deg;
        if(angle.IsBetween(-45, 45, true) && this.currentDirection != Direction.up) {
            this.currentDirection = Direction.up;
            this.animator.SetTrigger("walkUp");
        } else if(angle.IsBetween(45, 135) && this.currentDirection != Direction.right) {
            this.currentDirection = Direction.right;
            this.animator.SetTrigger("walkRight");
        } else if(angle.IsBetween(-225, -135, true) && this.currentDirection != Direction.down) {
            this.currentDirection = Direction.down;
            this.animator.SetTrigger("walkDown");
        } else if(angle.IsBetween(-135, -45) && this.currentDirection != Direction.left) {
            this.currentDirection = Direction.left;
            this.animator.SetTrigger("walkLeft");
        }
    }
}

