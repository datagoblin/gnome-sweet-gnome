﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour {

    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;
    public float facingAngleOffset = 0;

    //public LayerMask targetMask;
    public LayerMask obstacleMask;

    public GameObject player;

    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();

    public float meshResolution;
    public int edgeResolveIterations;
    public float edgeDstThreshold;

    public MeshFilter viewMeshFilter;
    Mesh viewMesh;

    public TriggerEnumeration triggerToSend;

    void Start() {
        //targetMask = LayerMask.GetMask("Player");
        viewMesh = new Mesh {
            name = "View Mesh"
        };
        viewMeshFilter.mesh = viewMesh;

        StartCoroutine("FindTargetsWithDelay", .05f);
    }

    IEnumerator FindTargetsWithDelay(float delay) {
        while (true) {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    void LateUpdate() {
        DrawFieldOfView();
    }

    public float MANGLE;
    void FindVisibleTargets() {
        bool playerFound = false;
        //visibleTargets.Clear();
        //Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        //for (int i = 0; i < targetsInViewRadius.Length; i++) {
        //    Transform target = targetsInViewRadius[i].transform;
        //    Vector2 dirToTarget = (target.position - transform.position).normalized;
        //    if (Vector2.Angle(transform.forward, dirToTarget) < viewAngle / 2) {
        //        float dstToTarget = Vector3.Distance(transform.position, target.position);
        //        if (!Physics2D.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask)) {
        //            visibleTargets.Add(target);
        //        }
        //    }
        //}
        float dist = Physics2D.Distance(player.GetComponent<Collider2D>(), GetComponent<Collider2D>()).distance;
        if (dist < viewRadius) {
            Vector2 dirToTarget = (player.transform.position - transform.position).normalized;
            if (Vector2.Angle(new Vector2(Mathf.Sin(Mathf.Deg2Rad * facingAngleOffset),Mathf.Cos(Mathf.Deg2Rad * facingAngleOffset)), dirToTarget) < viewAngle / 2) {
                if (!Physics2D.Raycast(transform.position, dirToTarget, dist, obstacleMask) && !player.GetComponent<PlayerFreeMovement>().isHidden) {
                    playerFound = true;
                }
            }
        }

        if (playerFound) {
            //LOSE
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GamePersonWhoDoesManagement>().Lose();
            CentralTriggers.SharedInstance.Trigger(this.triggerToSend);
        }


    }

    void DrawFieldOfView() {
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int i = 0; i <= stepCount; i++) {
            float angle = (transform.eulerAngles.z + facingAngleOffset) - viewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);
            Vector2 viewCastPoint = this.ViewCastPoint(i, stepAngleSize, oldViewCast, newViewCast);
            viewPoints.Add(viewCastPoint);
            oldViewCast = newViewCast;
        }
        this.UpdateViewMesh(viewPoints);
    }

    Vector2 ViewCastPoint(int index, float angleSizeStep, ViewCastInfo oldViewCast, ViewCastInfo viewCast) {
        if (index > 0) {
            bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - viewCast.dst) > this.edgeDstThreshold;
            if (oldViewCast.hit != viewCast.hit || (oldViewCast.hit && viewCast.hit && edgeDstThresholdExceeded)) {
                EdgeInfo edge = FindEdge(oldViewCast, viewCast);
                if (edge.pointA != Vector2.zero) {
                    return edge.pointA;
                }
                if (edge.pointB != Vector2.zero) {
                    return edge.pointB;
                }
            }
        }
        return viewCast.point;
    }

    void UpdateViewMesh(List<Vector3> viewPoints) {
        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++) {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);
            if (i < vertexCount - 2) {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();

        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast) {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++) {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded) {
                minAngle = angle;
                minPoint = newViewCast.point;
            } else {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    ViewCastInfo ViewCast(float globalAngle) {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, viewRadius, obstacleMask);

        if (hit) {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        } else {
            return new ViewCastInfo(false, transform.position + dir * viewRadius, viewRadius, globalAngle);
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
        if (!angleIsGlobal) {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), 0);
    }
}