﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CastLight : MonoBehaviour {

    public float lightRadius;
    private float currentLightRadius;
    [Range(0, 360)]
    public float lightAngle;
    private float currentLightAngle;
    public float lightAngleOffset = 0;

    [Range(0, 100)]
    public float flickerPercent = 0;

    public LayerMask obstacleMask;

    public float meshResolution;
    public int edgeResolveIterations;
    public float edgeDstThreshold;

    public MeshFilter viewMeshFilter;
    Mesh viewMesh;
    PolygonCollider2D poly;

    void Start() {
        this.CreateMesh();
        poly = GetComponent<PolygonCollider2D>();
        currentLightRadius = lightRadius;
        currentLightAngle = lightAngle;
        StartCoroutine("FlickerLightsWithDelay", .1f);
    }

    void CreateMesh() {
        viewMesh = new Mesh {
            name = "View Mesh"
        };
        viewMeshFilter.mesh = viewMesh;
    }
   
    IEnumerator FlickerLightsWithDelay(float delay) {
        while (true) {
            yield return new WaitForSeconds(delay);
            FlickerLight();
        }
    }

    void FlickerLight() {
        float actualPercent = Random.Range(0, flickerPercent) / 100;
        currentLightRadius = lightRadius - (actualPercent * lightRadius);
        currentLightAngle = lightAngle - (actualPercent * lightAngle);
    }

    void LateUpdate() {
        DrawFieldOfView();
    }

    Vector2 ViewCastPoint(int index, float angleSizeStep, ViewCastInfo oldViewCast, ViewCastInfo viewCast) {
        if(index > 0) {
            bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - viewCast.dst) > this.edgeDstThreshold;
            if (oldViewCast.hit != viewCast.hit || (oldViewCast.hit && viewCast.hit && edgeDstThresholdExceeded)) {
                EdgeInfo edge = FindEdge(oldViewCast, viewCast);
                if (edge.pointA != Vector2.zero) {
                    return edge.pointA;
                }
                if (edge.pointB != Vector2.zero) {
                    return edge.pointB;
                }
            }
        }
        return viewCast.point;
    }

    void UpdateViewMesh(List<Vector3> viewPoints) {
        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++) {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);
            if (i < vertexCount - 2) {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();

        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    void DrawFieldOfView() {
        int stepCount = Mathf.RoundToInt(currentLightAngle * meshResolution);
        float stepAngleSize = currentLightAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int i = 0; i <= stepCount; i++) {
            float correctedTransformAngle = (this.transform.eulerAngles.z + this.lightAngleOffset);
            float angle = correctedTransformAngle - this.currentLightAngle / 2 + stepAngleSize * i;
            ViewCastInfo viewCast = ViewCast(angle);
            Vector2 newViewCastPoint = this.ViewCastPoint(i, stepAngleSize, oldViewCast, viewCast);
            viewPoints.Add(newViewCastPoint);
            oldViewCast = viewCast;
        }
        Vector2[] polygon = new Vector2[viewPoints.Count + 1];
        polygon[0] = Vector2.zero;
        for (int i = 1; i < viewPoints.Count; i++) {
            polygon[i] = transform.InverseTransformPoint(new Vector2(viewPoints[i].x, viewPoints[i].y));
        }
        poly.SetPath(0, polygon);
        this.UpdateViewMesh(viewPoints);

    }

    //void DrawFieldOfView() {
    //    int stepCount = Mathf.RoundToInt(currentLightAngle * meshResolution);
    //    float stepAngleSize = currentLightAngle / stepCount;
    //    List<Vector3> viewPoints = new List<Vector3>();
    //    ViewCastInfo oldViewCast = new ViewCastInfo();
    //    for (int i = 0; i <= stepCount; i++) {
    //        float angle = (transform.eulerAngles.z + lightAngleOffset) - currentLightAngle / 2 + stepAngleSize * i;
    //        ViewCastInfo newViewCast = ViewCast(angle);

    //        if (i > 0) {
    //            bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
    //            if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded)) {
    //                EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
    //                if (edge.pointA != Vector2.zero) {
    //                    viewPoints.Add(edge.pointA);
    //                }
    //                if (edge.pointB != Vector2.zero) {
    //                    viewPoints.Add(edge.pointB);
    //                }
    //            }

    //        }


    //        viewPoints.Add(newViewCast.point);
    //        oldViewCast = newViewCast;
    //    }
    //    Vector2[] polygon = new Vector2[viewPoints.Count + 1];
    //    polygon[0] = Vector2.zero;
    //    for (int i = 1; i < viewPoints.Count; i++) {
    //        polygon[i] = transform.InverseTransformPoint(new Vector2(viewPoints[i].x, viewPoints[i].y));
    //    }
    //    poly.SetPath(0, polygon);

    //    int vertexCount = viewPoints.Count + 1;
    //    Vector3[] vertices = new Vector3[vertexCount];
    //    int[] triangles = new int[(vertexCount - 2) * 3];

    //    vertices[0] = Vector3.zero;
    //    for (int i = 0; i < vertexCount - 1; i++) {
    //        vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

    //        if (i < vertexCount - 2) {
    //            triangles[i * 3] = 0;
    //            triangles[i * 3 + 1] = i + 1;
    //            triangles[i * 3 + 2] = i + 2;
    //        }
    //    }

    //    viewMesh.Clear();

    //    viewMesh.vertices = vertices;
    //    viewMesh.triangles = triangles;
    //    viewMesh.RecalculateNormals();

    //}


    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast) {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++) {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded) {
                minAngle = angle;
                minPoint = newViewCast.point;
            } else {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }


    ViewCastInfo ViewCast(float globalAngle) {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, currentLightRadius, obstacleMask);

        if (hit) {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        } else {
            return new ViewCastInfo(false, transform.position + dir * currentLightRadius, currentLightRadius, globalAngle);
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
        if (!angleIsGlobal) {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), 0);
    }
}
