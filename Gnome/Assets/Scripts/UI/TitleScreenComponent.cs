﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenComponent : MonoBehaviour {
    public Sprite[] imageOrder;
    public string nextSceneName;
    private SpriteRenderer sr;
    private int index = 0;

    void Start() {
        this.sr = this.GetComponent<SpriteRenderer>();
    }

    void Update() {
        if (Input.anyKeyDown && index != 0) {
            Next();
        }
    }

    public void Next() {
        if(index == 0) {
            Destroy(GameObject.Find("Canvas"));
        }

        if (index < this.imageOrder.Length) {
            this.sr.sprite = imageOrder[index++];
        } else {
            SceneManager.LoadScene(SceneCoordinator.SceneName(SceneEnum.FirstStage));
        }
    }
}
