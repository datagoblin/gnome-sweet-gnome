﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class UserInteractableMenu : MonoBehaviour {
    public bool goToIntro = false;

    // Update is called once per frame
    void Update() {
        if(Input.anyKeyDown) {
            if(goToIntro) {
                SceneManager.LoadScene(SceneCoordinator.SceneName(SceneEnum.TitleScene));
            } else {
                this.GoToNextAvailableStage();
            }
        }
    }

    void GoToNextAvailableStage() {
        string currentScene = SceneManager.GetActiveScene().name;
        string stageName;
        if(currentScene == SceneCoordinator.SceneName(SceneEnum.WinScene)) {
            stageName = SceneCoordinator.SceneName(SceneEnum.NextStage);
        } else {
            stageName = SceneCoordinator.SceneName(SceneEnum.LastPlayedStage);
        }
        SceneManager.LoadScene(stageName);
    }
}
