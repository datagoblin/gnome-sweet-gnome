﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideBoardComponent : MonoBehaviour {
    public Text timerText;
    public CanvasRenderer questPanel;
    public BoardEntry questEntryPrefab;

    private void Start() {
    }

    private void FixedUpdate() {
        this.SetTimer(Time.timeSinceLevelLoad);
    }

    public void SetTimer(float timer) {
        string minutes = Mathf.Floor(timer / 59).ToString("00");
        string seconds = (timer % 59).ToString("00");
        this.timerText.text = minutes + ":" + seconds;
    }

    public void SetBoardEntries(QuestEntry[] entries) { 
        this.questPanel.transform.DetachChildren();
        for (int i = 0; i < entries.Length; i++) {
            this.AddBoardEntry(entries[i]);
        }
    }

    public void AddBoardEntry(QuestEntry entry) {
        BoardEntry be = Instantiate(questEntryPrefab);
        be.text.text = entry.info;
        be.transform.SetParent(questPanel.transform);
        RectTransform rt = be.GetComponent<RectTransform>();
        rt.localScale = Vector3.one;
    }
}
