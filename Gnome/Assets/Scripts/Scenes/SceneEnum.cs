﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneEnum {
    None, TitleScene, WinScene, CreditsScene, LoseScene, FirstStage, NextStage, PreviousStage, LastPlayedStage
}
