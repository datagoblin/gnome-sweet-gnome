﻿using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class SceneCoordinator {
    public static int CurrentStage {
        get {
            string sceneName = SceneManager.GetActiveScene().name;
            string resultString = Regex.Match(sceneName, @"\d+").Value;
            return Int32.Parse(resultString);
        }
    }
    private static readonly string stageScenePrefix = "Stage";
    public static string Stage(int number) {
        return stageScenePrefix + (number.ToString("00"));
    }

    public static int lastPlayedStage = 0;

    public static string SceneName(SceneEnum scene) {
        switch (scene) {
            case SceneEnum.FirstStage:
                return Stage(1);
            case SceneEnum.NextStage:
                return Stage(lastPlayedStage + 1);
            case SceneEnum.PreviousStage:
                return Stage(lastPlayedStage - 1);
            case SceneEnum.LastPlayedStage:
                return Stage(lastPlayedStage);
            case SceneEnum.WinScene:
                return "WinScene";
            case SceneEnum.LoseScene:
                return "LoseScene";
            case SceneEnum.TitleScene:
                return "TitleScene";
            case SceneEnum.CreditsScene:
                return "CreditsScenes";
            default:
                return null;
        }
    }
}
