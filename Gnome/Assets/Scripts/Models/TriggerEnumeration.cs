﻿using System;

public enum TriggerEnumeration {
    None,
    ObjectivesComplete,
    PlayerFled,
    PlayerDied,
    PlayerPickedItem,
    PlayerDroppedItem,
    PlayerFixedItem
}