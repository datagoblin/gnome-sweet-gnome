﻿using UnityEngine;
using System;

[System.Serializable]
public class Waypoint {
    public Vector2 position;
    public float waitTime;

    public Waypoint(Vector2 pos, float time) {
        position = pos;
        waitTime = time;
    }
}