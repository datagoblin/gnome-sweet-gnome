﻿using System;

public struct QuestEntry {
    public bool isCompleted;
    public string info;

    public QuestEntry(bool isCompleted, string info) {
        this.isCompleted = isCompleted;
        this.info = info;
    }
}