﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

[System.Serializable]
public struct SpecialEffect {
    public AudioClip specialEffect;
    public TriggerEnumeration trigger;
    public bool shouldStopMuisc;
}

public class MusicControl : MonoBehaviour, ITriggerObserver {
    public float transitionTime = 0.1f;
    public AudioMixerSnapshot outOfCombat;
    public AudioMixerSnapshot inCombat;
    public AudioMixerSnapshot same;
    public SpecialEffect[] specialEffects;
    public AudioSource musicSource1;
    public AudioSource musicSource2;
    public AudioSource specialEffectsSource;

    // Use this for initialization
    void Start() {
        foreach(SpecialEffect sfx in this.specialEffects) {
            CentralTriggers.SharedInstance.AddObserver(this, sfx.trigger);
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.A)) {
            //same.TransitionTo(transitionTime);
            //this.StartCoroutine("changeTrack", inCombat);

            this.specialEffectsSource.PlayOneShot(this.specialEffects[0].specialEffect);
        }

        //if (Input.GetKeyUp(KeyCode.A)) {
        //    same.TransitionTo(transitionTime);
        //    this.StartCoroutine("changeTrack", outOfCombat);
        //}
    }

    IEnumerator changeTrack(AudioMixerSnapshot snp) {
        bool running = true;
        while (running) {
            yield return new WaitForSeconds(1.0f);
            running = false;
        }
        snp.TransitionTo(transitionTime * 3);
    }

    public void OnTrigger(TriggerEnumeration trigger) {
        foreach(SpecialEffect sfx in this.specialEffects) {
            if(sfx.trigger.Equals(trigger)) {
                Debug.Log("test");
                this.specialEffectsSource.PlayOneShot(sfx.specialEffect);
                if(sfx.shouldStopMuisc) {
                    musicSource1.Stop();
                    musicSource2.Stop();
                }
            }
        }
    }
}