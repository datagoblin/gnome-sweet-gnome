<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="objects" tilewidth="16" tileheight="16" tilecount="20" columns="5">
 <image source="objects.png" width="80" height="64"/>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
   <object id="5" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="13">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0">
    <polygon points="0,0 16,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 16,16 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
